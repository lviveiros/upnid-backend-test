require('dotenv').load();
  const stringdeconnection = process.env.JAWSDB_URL;
module.exports = {
  migrations: {
    tableName: 'knex_migrations'
  },
  seeds: {
    tableName: './user_seed'
  },
  client: process.env.DB_DIALECT,
  connection: stringdeconnection
};