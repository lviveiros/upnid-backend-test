import Hapi from 'hapi';
import Jwt from 'hapi-auth-jwt';
import Routes from './routes/routes';
import dotenv from 'dotenv';
import Vision from 'vision';
import Inert from 'inert';
import Lout from 'lout';

const server = new Hapi.Server();
const port = process.env.PORT || 8080;
server.connection({
  routes: {
    cors: true
  },
  port: port
});

server.on('response', function (request) {
  console.log(request.info.remoteAddress + ': ' + request.method.toUpperCase() + ' ' + request.url.path + ' --> ' + request.response.statusCode);
});

server.register(
  [Vision,
    Inert,
    Lout,
    Jwt,
  ],
  err => {
    if (!err) {
      console.log('registered plugins');
    }
    dotenv.load();

    server.auth.strategy('token', 'jwt', {
      key: process.env.ENCRYPTION_KEY,
      verifyOptions: {
        algorithms: ['HS256']
      }
    });

    const routes = new Routes(server);
    routes.cardRoutes();
    routes.userRoutes();
    routes.auth();

    server.start(err => {
      if (err) {
        console.error('Error was handled!');
        console.error(err);
      }
      console.log(`Server started at ${server.info.uri}`);
    });
  },
);

export default server;