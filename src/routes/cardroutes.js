import Knex from '../knex';
import Boom from 'boom';
import Card from '../models/card';
import Joi from 'joi';
import Encryption from '../encryption';

class CardRoutes {
  constructor(server) {
    this.server = server;
  }

  setupRoutes() {
    this.server.route({
      method: 'GET',
      path: '/cards',
      handler: this.index,
      config: {
        auth: {
          strategy: 'token'
        }
      },
    });

    this.server.route({
      method: 'POST',
      path: '/card',
      handler: this.newCard,
      config: configPostRoute
    });

    this.server.route({
      method: 'PUT',
      path: '/card/{cardid}',
      handler: this.editCard,
      config: configPutRoute
    });

    this.server.route({
      method: 'DELETE',
      path: '/card/{cardid}',
      handler: this.deleteCard,
      config: configDeleteRoute
    });
  }

  async index(request, reply) {
    const enc = new Encryption();
    Knex('card').
    select(Card.COLUMN_ID, Card.COLUMN_CARDNUMBER, Card.COLUMN_CVV, Card.COLUMN_USERID, Card.COLUMN_COMPANY, Card.COLUMN_EXPIRATION_DATE).
    where({
      userid: request.auth.credentials.userid
    }).
    then((results) => {
      if (!results || results.length === 0) {
        return reply().code(204);
      }

      for (let card of results) {
        card.cardnumber = enc.decrypt(card.cardnumber);
        card.cvv = enc.decrypt(card.cvv);
      }

      reply({
        dataCount: results.length,
        data: results,
      });

    }).catch((err) => {
      reply(Boom.badImplementation());
    });
  }

  newCard(request, reply) {
    const enc = new Encryption();
    let insertData = {
      cardnumber: enc.encrypt(request.payload.cardnumber),
      cvv: enc.encrypt(request.payload.cvv),
      expiration_date: request.payload.expiration_date,
      company: request.payload.company,
      userid: request.auth.credentials.userid,
      created_at: new Date(),
    }

    Knex('card').
    select(Card.COLUMN_CARDNUMBER).
    then((cards) => {

      if (cards.length !== 0) {
        let validCard = false;
        for (let card of cards) {
          if (enc.decrypt(card.cardnumber) === request.payload.cardnumber) {
            return reply(Boom.badRequest('credit card already exists'));
          }
        }
      }

      Knex('card').
      returning(Card.COLUMN_ID).
      insert(insertData).
      then((results) => {
        if (!results || results.length === 0) {
          return reply(Boom.badImplementation());
        }

        delete insertData['created_at'];
        delete insertData['userid'];
        insertData['cardid'] = results[0].cardid;

        reply().code(200);
      }).catch((err) => {
        return reply(Boom.badImplementation('failed to save card'));
      });


    }).catch((err) => {
      return reply(Boom.badImplementation());
    });
  }



  editCard(request, reply) {
    const enc = new Encryption();
    Knex('card').
    where({
      cardid: request.params.cardid
    }).
    select(Card.COLUMN_USERID, Card.COLUMN_CARDNUMBER).
    then(result => {

      if (!result || result.length === 0) {
        return reply(Boom.badRequest('no cards yet'));
      } else {
        if (result[0].userid !== request.auth.credentials.userid) {
          return reply(Boom.unauthorized('can only update logged users cards'));
        } else {

          Knex('card').
          whereNot({
            cardid: request.params.cardid
          }).
          select(Card.COLUMN_CARDNUMBER).
          then(actualCards => {

            let existingSameNumberCard = 0;
            for (let actualCard of actualCards) {
              if (request.payload.cardnumber === enc.decrypt(actualCard.cardnumber)) {
                return reply(Boom.badRequest('credit card already exists'));
              }
            }

            const updateData = {
              cardnumber: enc.encrypt(request.payload.cardnumber),
              cvv: enc.encrypt(request.payload.cvv.toString()),
              expiration_date: request.payload.expiration_date,
              company: request.payload.company,
              userid: request.auth.credentials.userid,
              updated_at: new Date(),
            }

            Knex('card').
            where({
              cardid: request.params.cardid
            }).
            update(updateData).
            then((count) => {
              if (!count || count === 0) {
                return reply(Boom.badRequest('card not found'));
              }
              reply().code(200);
            }).catch((err) => {
              return reply(Boom.badImplementation('failed to save card'));
            });

          });
        }
      }
    });

  };

  deleteCard(request, reply) {

    Knex('card').
    where({
      cardid: request.params.cardid
    }).
    select(Card.COLUMN_USERID, Card.COLUMN_CARDNUMBER).
    then(result => {
      if (!result || result.length === 0) {
        return reply(Boom.badRequest('no cards yet'));
      } else {
        if (result[0].userid !== request.auth.credentials.userid) {
          return reply(Boom.badRequest('cannot delete other users cards'));
        } else {
          Knex('card').
          where({
            cardid: request.params.cardid
          }).
          del().
          then((count) => {
            if (!count || count === 0) {
              return reply(Boom.badRequest('Card not found'));
            }
            return reply().code(200);
          }).catch((err) => {
            return reply(Boom.badImplementation('failed to delete user'));
          });
        }
      }
    });
  }

}


const configPostRoute = {
  validate: {
    payload: {
      cardnumber: Joi.string().creditCard().required(),
      cvv: Joi.string().alphanum().length(3).required(),
      expiration_date: Joi.date().iso().required(),
      company: Joi.string().alphanum().min(2).max(255).required(),
    },
  },
  auth: {
    strategy: 'token'
  }
}

const configPutRoute = {
  validate: {
    payload: {
      cardid: Joi,
      cardnumber: Joi.string().creditCard(),
      cvv: Joi.string().regex(/^[0-9]{3}$/),
      expiration_date: Joi.date().iso().required(),
      company: Joi.string().alphanum().min(2).max(255).required(),
    },
  },
  auth: {
    strategy: 'token'
  }
}

const configDeleteRoute = {
  auth: {
    strategy: 'token'
  }
}




export default CardRoutes;