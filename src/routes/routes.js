import Knex from '../knex';
import jwt from 'jsonwebtoken';
import Boom from 'boom';
import CardRoutes from './cardroutes';
import UserRoutes from './userroutes';
import IndexRoute from './indexroutes';
import Auth from './authroutes';

class Routes {
  constructor(server) {
    this.server = server;
    this.index();
  }

  index() {
    new IndexRoute(this.server).setupRoute();
  }

  cardRoutes() {
    new CardRoutes(this.server).setupRoutes();
  }

  userRoutes() {
    new UserRoutes(this.server).setupRoutes();
  }

  auth() {
    new Auth(this.server).auth();
  }


}


export default Routes;