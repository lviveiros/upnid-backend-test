import Knex from '../knex';
import Boom from 'boom';
import User from '../models/user';
import Joi from 'joi';
import Encryption from '../encryption';


class UserRoutes {
  constructor(server) {
    this.server = server;
  }

  setupRoutes() {
    this.server.route({
      method: 'GET',
      path: '/users',
      handler: this.index,
      config: {
        auth: {
          strategy: 'token'
        }
      },
    });

    this.server.route({
      method: 'GET',
      path: '/me',
      handler: this.getMe,
      config: {
        auth: {
          strategy: 'token'
        }
      },
    });

    this.server.route({
      method: 'POST',
      path: '/user',
      handler: this.newUser,
      config: configPostRoute
    });

    this.server.route({
      method: 'PUT',
      path: '/user/{userid}',
      handler: this.editUser,
      config: configPutRoute
    });


    this.server.route({
      method: 'DELETE',
      path: '/user/{userid}',
      handler: this.deleteUser,
      config: configDeleteRoute
    });

  }

  async index(request, reply) {

    Knex('users').
    select(User.COLUMN_NAME, User.COLUMN_EMAIL, User.COLUMN_ID, User.COLUMN_ROLE).
    then((results) => {
      if (!results || results.length === 0) {
        reply({
          error: true,
          errMessage: 'no user found',
        });
      }
      reply({
        dataCount: results.length,
        data: results,
      });

    }).catch((err) => {
      reply('server-side error ');
    });
  }

  newUser(request, reply) {

    let userBeingCreated = {
      email: request.payload.email,
      name: request.payload.name,
      surname: request.payload.surname,
      role: 'member',
      created_at: new Date(),
      password: new Encryption().encrypt(request.payload.password)
    }
    Knex('users').
    returning(User.COLUMN_ID).
    insert(userBeingCreated).
    then((results) => {

      userBeingCreated['cardid'] = results[0];
      delete userBeingCreated.password;
      delete userBeingCreated.created_at;

      if (!results || results.length === 0) {
        return reply({
          error: true,
          errMessage: 'no user inserted',
        });
      } else {
        return reply(userBeingCreated).code(200);
      }
    }).catch((err) => {
      if (err.code === 'ER_DUP_ENTRY') {
        reply(Boom.badRequest('duplicate email'));
      } else {
        reply(Boom.badImplementation('failed to insert user'));
      }
    });
  }

  editUser(request, reply) {
    if ((request.auth.credentials.userid !== parseInt(request.params.userid)) || request.auth.credentials.userRole === 'admin') {
      return reply(Boom.unauthorized('users can only update themselves'));
    }
    Knex('users').where({
      userid: request.params.userid,
    }).
    update({
      name: request.payload.name,
      surname: request.payload.surname,
      email: request.payload.email,
      updated_at: new Date(),
    }).
    then((count) => {
      if (!count || count === 0) {
        reply(Boom.badRequest('User not found'));
      }
      reply().code(200);
    }).catch((err) => {
      if (err.code === 'ER_DUP_ENTRY') {
        reply(Boom.badRequest('duplicate email'));
      } else {
        reply(Boom.badImplementation('failed to update user'));
      }
    });
  }

  deleteUser(request, reply) {
    if (request.auth.credentials.userid !== parseInt(request.params.userid)) {
      return reply(Boom.unauthorized('users can only delete themselves'));
    }
    Knex('users').where({
      userid: request.params.userid,
    }).
    del().
    then((count) => {
      if (!count || count === 0) {
        return reply(Boom.badRequest('User not found'));
      }
      return reply().code(200);
    }).catch((err) => {
      if (err.code === 'ER_ROW_IS_REFERENCED_2') {
        return reply(Boom.badRequest('cannot delete user with cards'));
      }
      return reply(Boom.badImplementation('failed to delete user'));
    });
  }

  getMe(request, reply) {
    Knex('users').
    where({
      userid: request.auth.credentials.userid
    }).
    select(User.COLUMN_NAME, User.COLUMN_SURNAME, User.COLUMN_EMAIL, User.COLUMN_ID, User.COLUMN_ROLE).
    then((results) => {
      if (!results || results.length === 0) {
        reply({
          error: true,
          errMessage: 'no user found',
        });
      }
      reply(
        results[0]
      );

    }).catch((err) => {
      reply(Boom.badImplementation('failed to connect to the database'))
    });
  }

}

const configPostRoute = {
  validate: {
    payload: {
      email: Joi.string().email().required(),
      name: Joi.string().alphanum().min(2).max(30).required(),
      surname: Joi.string().alphanum().min(2).max(30).required(),
      password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
    },
  },

}

const configPutRoute = {
  validate: {
    payload: {
      userid: Joi,
      email: Joi.string().email().required(),
      name: Joi.string().alphanum().min(2).max(30).required(),
      surname: Joi.string().alphanum().min(2).max(30).required(),
    },
  },
  auth: {
    strategy: 'token'
  }
}

const configDeleteRoute = {
  auth: {
    strategy: 'token'
  }
}

export default UserRoutes;