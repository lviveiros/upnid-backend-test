import Knex from '../knex';
import Joi from 'joi';
import User from '../models/user';
import Boom from 'boom';
import dotEnv from 'dotenv';
import Encryption from '../encryption';
import jwt from 'jsonwebtoken';

class IndexRoutes {
  constructor(server) {
    this.server = server;
    dotEnv.load();
  }

  setupRoute() {
    this.server.route({
      path: '/',
      method: 'GET',
      handler: (request, reply) => {
        reply().redirect('/docs');
      }
    });
  }
}

export default IndexRoutes;