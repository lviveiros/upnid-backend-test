import Knex from '../knex';
import Joi from 'joi';
import User from '../models/user';
import Boom from 'boom';
import dotEnv from 'dotenv';
import Encryption from '../encryption';
import jwt from 'jsonwebtoken';

class Auth {
  constructor(server) {
    this.server = server;
    dotEnv.load();
  }

  auth() {
    this.server.route({
      path: '/auth',
      method: 'POST',
      config: {
        validate: {
          payload: {
            email: Joi.string().email().required(),
            password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
          },
        }
      },
      handler: (request, reply) => {
        const {
          email,
          password
        } = request.payload;

        const getOperation = Knex('users').where({
          email,
        }).select(User.COLUMN_ID, User.COLUMN_PASSWORD, User.COLUMN_ROLE).then(([user]) => {
          if (!user) {
            return reply(Boom.unauthorized());
          }

          const userid = user.userid;
          const userRole = user.role;
          const actualPassword = new Encryption().decrypt(user.password);
          const inputPassword = request.payload.password;

          if (actualPassword === inputPassword) {
            const token = jwt.sign({
              userid,
              userRole,
            }, process.env.ENCRYPTION_KEY, {
              algorithm: 'HS256',
              expiresIn: '1h',
            });
            reply({
              token,
            });
          } else {
            return reply(Boom.unauthorized());
          }
        }).catch((err) => {
          return reply(Boom.badImplementation());
        });
      }
    });
  }
}

export default Auth;