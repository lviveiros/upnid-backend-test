class Card {
  constructor(cardData) {
    this.id = cardData.id;
    this.owner = cardData.owner;
    this.cardnumber = cardData.cardnumber;
    this.cvv = cardData.cvv;
    this.expiration_date = cardData.expiration_date;
    this.company = cardData.company;
    this.created_at = cardData.created_at;
    this.guid = cardData.guid;
  }
}

Card.COLUMN_ID = 'cardid';
Card.COLUMN_CARDNUMBER = 'cardnumber';
Card.COLUMN_CVV = 'cvv';
Card.COLUMN_USERID = 'userid';
Card.COLUMN_COMPANY = 'company';
Card.COLUMN_EXPIRATION_DATE = 'expiration_date';
Card.COLUMN_GUID = 'guid';

export default Card;