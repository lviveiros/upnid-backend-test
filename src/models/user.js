class User {
  constructor(userData) {
    this.id = userData.id;
    this.name = userData.name;
    this.email = userData.email;
    this.password = userData.password;
    this.created_at = userData.created_at;
    this.guid = userData.guid;
  }
}

User.COLUMN_ID = 'userid';
User.COLUMN_ROLE = 'role';
User.COLUMN_NAME = 'name';
User.COLUMN_SURNAME = 'surname';
User.COLUMN_EMAIL = 'email';
User.COLUMN_PASSWORD = 'password';
User.COLUMN_CREATED_AT = 'created_at';

export default User;