import crypto from 'crypto';
import dotEnv from 'dotenv';

class Encryption {
  constructor() {
    dotEnv.load();
    this.ENCRYPTION_KEY = process.env.ENCRYPTION_KEY;
    this.IV_LENGTH = 16;
  }

  encrypt(text) {
    let iv = crypto.randomBytes(this.IV_LENGTH);
    let cipher = crypto.createCipheriv('aes-256-cbc', new Buffer(this.ENCRYPTION_KEY), iv);
    let encrypted = cipher.update(text);

    encrypted = Buffer.concat([encrypted, cipher.final()]);

    return iv.toString('hex') + ':' + encrypted.toString('hex');
  }

  decrypt(text) {
    let textParts = text.split(':');
    let iv = new Buffer(textParts.shift(), 'hex');
    let encryptedText = new Buffer(textParts.join(':'), 'hex');
    let decipher = crypto.createDecipheriv('aes-256-cbc', new Buffer(this.ENCRYPTION_KEY), iv);
    let decrypted = decipher.update(encryptedText);

    decrypted = Buffer.concat([decrypted, decipher.final()]);

    return decrypted.toString();
  }
}

export default Encryption;