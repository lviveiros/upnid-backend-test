exports.up = function (knex, Promise) {
  return knex
    .schema
    .createTable('users', function (usersTable) {

      // Primary Key
      usersTable.increments('userid').primary().unsigned;

      // Data
      usersTable.string('name', 30).notNullable();
      usersTable.string('role', 6).notNullable();
      usersTable.string('surname', 50).notNullable();
      usersTable.string('email', 250).notNullable().unique();
      usersTable.string('password', 1000).notNullable();
      usersTable.timestamp('created_at').notNullable();
      usersTable.timestamp('updated_at').notNullable();

    }).createTable('card', function (cardsTable) {


      cardsTable.increments('cardid').primary();
      cardsTable.integer('userid').unsigned().references('userid').inTable('users');

      cardsTable.string('cardnumber', 1000).notNullable();
      cardsTable.string('cvv', 1000).notNullable();
      cardsTable.string('expiration_date').notNullable();
      cardsTable.string('company', 1000).notNullable();
      cardsTable.timestamp('created_at').notNullable();
      cardsTable.timestamp('updated_at').notNullable();
    });

};

exports.down = function (knex, Promise) {
  return knex.schema
    .dropTable('card')
    .dropTable('users')
};