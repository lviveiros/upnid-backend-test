import test from 'ava';
import server from '../src/server';
import jwt from 'jsonwebtoken';
import Knex from '../src/knex';
import Encryption from '../src/encryption';


test('endpoint test | POST /card | valid card -> 200 success with valid card', t => {
  const userid = 2;
  const userRole = 'member';

  const token = jwt.sign({
    userid,
    userRole,
  }, process.env.ENCRYPTION_KEY, {
    algorithm: 'HS256',
    expiresIn: '1h',
  });
  const bearerToken = 'Bearer ' + token;

  const requestCard = {
    method: 'POST',
    url: '/card',
    payload: {
      cardnumber: '379156879837782',
      cvv: '123',
      company: 'testcompany',
      expiration_date: '9999-10-10 00:00:00',
    },
    headers: {
      authorization: bearerToken
    }
  };

  return server.inject(requestCard)
    .then(response => {
      t.is(response.statusCode, 200, 'status code is 200 post card');
    });
});

test('endpoint test | POST /card | invalid card -> 400 fail', t => {
  const userid = 2;
  const userRole = 'member';

  const token = jwt.sign({
    userid,
    userRole,
  }, process.env.ENCRYPTION_KEY, {
    algorithm: 'HS256',
    expiresIn: '1h',
  });
  const bearerToken = 'Bearer ' + token;

  const requestCard = {
    method: 'POST',
    url: '/card',
    payload: {
      cardnumber: '12341234',
      cvv: '123',
      company: 'testcompany',
      expiration_date: '9999-10-10 00:00:00',
    },
    headers: {
      authorization: bearerToken
    }
  };

  return server.inject(requestCard)
    .then(response => {
      t.is(response.statusCode, 400, 'status code is 400 post card');
      t.is(response.result.error, 'Bad Request', 'Error is BadRequest');
    });
});

test.after.always('remove card created', t => {
  return Knex('card').where({
    company: 'testcompany'
  }).del();
});