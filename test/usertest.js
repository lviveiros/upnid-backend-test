import test from 'ava';
import server from '../src/server';
import jwt from 'jsonwebtoken';
import Knex from '../src/knex';


test('endpoint test | GET /me | authentication token -> 200 success with user one data', t => {
  const userid = 1;
  const userRole = 'member';

  const token = jwt.sign({
    userid,
    userRole,
  }, process.env.ENCRYPTION_KEY, {
    algorithm: 'HS256',
    expiresIn: '1h',
  });
  const bearerToken = 'Bearer ' + token;
  const requestMe = {
    method: 'GET',
    url: '/me',
    headers: {
      authorization: bearerToken
    }
  };

  return server.inject(requestMe)
    .then(response => {
      const dataret = JSON.parse(response.payload)
      t.is(response.statusCode, 200, 'status code is 200 get Me');
      t.is(dataret.name, 'oneName', 'name validation');
      t.is(dataret.email, 'useremail@one.com', 'email validation');
      t.is(dataret.role, 'member', 'role validation');
    });
});


test('endpoint test | PUT /user/1 | authentication token user 1 -> 200 success user editted', t => {
  const userid = 1;
  const userRole = 'member';

  const token = jwt.sign({
    userid,
    userRole,
  }, process.env.ENCRYPTION_KEY, {
    algorithm: 'HS256',
    expiresIn: '1h',
  });
  const bearerToken = 'Bearer ' + token;

  const requestPut = {
    method: 'PUT',
    url: '/user/1',
    headers: {
      authorization: bearerToken
    },
    payload: {
      email: 'useremail@one.com',
      name: 'oneName',
      surname: 'oneSurname',
    }
  };

  return server.inject(requestPut)
    .then(response => {
      t.is(response.statusCode, 200, 'status code is 200 edit user');
    });
});

test('endpoint test | PUT /user/2 | authentication token user 1 -> 401 unauthorized', t => {
  const userid = 2;
  const userRole = 'member';

  const token = jwt.sign({
    userid,
    userRole,
  }, process.env.ENCRYPTION_KEY, {
    algorithm: 'HS256',
    expiresIn: '1h',
  });
  const bearerToken = 'Bearer ' + token;

  const requestPut = {
    method: 'PUT',
    url: '/user/1',
    headers: {
      authorization: bearerToken
    },
    payload: {
      email: 'useremail@one.con',
      name: 'oneName',
      surname: 'oneSurname',
      role: 'member',
    }
  };

  return server.inject(requestPut)
    .then(response => {
      t.is(response.statusCode, 400, 'status code is 400 edit user');
    });
});

test('endpoint test | DELETE /user/2 | authentication token user 1 -> 401 unauthorized', t => {
  const userid = 2;
  const userRole = 'member';

  const token = jwt.sign({
    userid,
    userRole,
  }, process.env.ENCRYPTION_KEY, {
    algorithm: 'HS256',
    expiresIn: '1h',
  });
  const bearerToken = 'Bearer ' + token;

  const requestDelete = {
    method: 'DELETE',
    url: '/user/1',
    headers: {
      authorization: bearerToken
    },
  };

  return server.inject(requestDelete)
    .then(response => {
      t.is(response.statusCode, 401, 'status code is 401 delete user');
    });
});

test('endpoint test | POST /user | duplicate email -> 400 fail with duplicate email', t => {
  const requestMe = {
    method: 'POST',
    url: '/user',
    payload: {
      email: 'useremail@one.com',
      name: 'oneName',
      surname: 'oneSurname',
      password: 'password',
    }
  };

  return server.inject(requestMe)
    .then(response => {
      const dataret = JSON.parse(response.payload)
      t.is(response.statusCode, 400, 'status code is 400');
      t.is(dataret.error, 'Bad Request');
      t.is(dataret.message, 'duplicate email');
    });
});


test('endpoint test | POST /user | valid user -> 200 success new user created', t => {
  const requestMe = {
    method: 'POST',
    url: '/user',
    payload: {
      email: 'useremail@three.com',
      name: 'threeName',
      surname: 'threeSurname',
      password: 'password',
    }
  };

  return server.inject(requestMe)
    .then(response => {
      t.is(response.statusCode, 200, 'status code is 200 /POST new user');
    });
});

test.after.always('remove user created', t => {
  return Knex('users').where({
    email: 'useremail@three.com'
  }).del();
});