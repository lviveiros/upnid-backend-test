exports.seed = function (knex, Promise) {
  return knex('card').del()
    .then(function () {
      var rows = [{
          cardid: 1,
          userid: 1,
          cardnumber: 'e4cdd153d046a15ed22be9e97e3528e8:6ffb4d01d6e5d6b7b3f1093888d6e16d8be3a098f05570762fd7da6c2b85f4f8',
          cvv: 'b0826645e4ea4032747770a4498d2d27:62ff6e897dc4e75a8368ada45805de64',
          expiration_date: '2020-01-01',
          company: 'upnid',
          created_at: '2018-04-28'
        },
  
        {
          cardid: 2,
          userid: 1,
          cardnumber: '12997ef16cfb7276f81e881c69da2221:bd7ad5c9651c56b9bc8cba20490e1607f6f81b05dd94491aa555bcf801ce0552',
          cvv: 'e03f70c917eabf2845b48436f81a982b:47c7e85ecdc3ba12c36a81a00be1d9dd',
          expiration_date: '2020-01-01',
          company: 'upnid',
          created_at: '2018-04-28'
        },
        {
          cardid: 3,
          userid: 1,
          cardnumber: '0156798b1bb0da9abe359822483f0c9b:d97eb25b05bc94dc5fbf298837f14f0bcb8ab1789e6ca32420d7c14afb1b1506',
          cvv: '6b9f15b47f1e532683f46be27745a704:144da6f0b12dfa67a20d2ad3ea84c17f',
          expiration_date: '2020-01-01',
          company: 'otherCompany',
          created_at: '2018-04-28'
        },
        {
          cardid: 4,
          userid: 2,
          cardnumber: '7598fff675422cf24210244b555915d3:54602964d16bdad90def58031242919474e831cb8e98ae8bc58084f4122fb40b',
          cvv: '7ec0634a214c67d9aa5c00a72a24ec8d:55b4b9ae0f701d088410914ed6925b9e',
          expiration_date: '2020-01-01',
          company: 'otherCompany',
          created_at: '2018-04-28'
        },
      ];
      return knex('card').insert(rows);
    });
};