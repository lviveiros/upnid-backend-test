exports.seed = function (knex, Promise) {

  return knex('users').del()
    .then(function () {

      var rows = [{
        userid: 1,
        name: 'oneName',
        surname: 'oneSurname',
        email: 'useremail@one.com',
        password: '85687469195d79a58e215e554839871f:0a7290844ce63396b0c2174b57adac73',
        role: 'member',
        created_at: new Date()
      }, {
        userid: 2,
        name: 'twoName',
        surname: 'twoSurname',
        email: 'useremail@two.com',
        password: '6fbf16d05047cd66f7077baafc9ea121:8538ab8eee0ec21f20d714eb485b6492',
        created_at: new Date()
      },
      {
        userid: 3,
        name: 'Leonardo',
        surname: 'Viveiros',
        email: 'leonardoviveirossantos@gmail.com',
        password: '6fbf16d05047cd66f7077baafc9ea121:8538ab8eee0ec21f20d714eb485b6492',
        created_at: new Date()
      }];
      return knex('users').insert(rows);
    });
};