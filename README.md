# Upnid Backend Test - Leonardo Viveiros

Rest service using Hapi js v16, Knex for ORM, MySQL for data storage running on JawsDB and Ava for unit testing  

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

The [Lout](https://github.com/hapijs/lout) Hapi Plugin was used for generate API documentation, found in /docs


## Documentation

The API documentation can be found [here](https://upnid-backend-test.herokuapp.com/docs).

### Prerequisites

- Software you need installed prior to execute the project

```
node
npm
```

- Setting ENV variables

```
JAWSDB_URL=My SQL URI connection
DB_DIALECT=mysql
ENCRYPTION_KEY=Super Secret Key used for encryptation and signing JWT token
```


### Database Migration and seeds

- There are some database migrations under /migrations folder that creates a basic structure

```
knex migrate:rollback
knex migrate:latest
```

- There's a seed file under /seeds folder that creates some users and some cards

```
knex seed:run
```

### Installing


- A step by step series of examples that tell you have to get a development env running

#### Clonning repository

```
git clone 
```


#### Downloading Dependencies

```
npm install
```

#### Running tests

- It's the tests are throwing error, run the database seed before running the tests

```
knex migrate:rollback
knex migrate:latest
knex seed:run
```
```
npm test
```



#### Start the server in Development mode
```
npm run dev
```

#### Start the server in Production mode
```
npm run start
```

## Deployment

The API up and running can be found on [Heroku](https://dashboard.heroku.com/apps/upnid-backend-test) 

## Built With

* [HapiJS](https://hapijs.com) - The web framework used
* [KnexJS](http://knexjs.org/) - ORM library used for the database layer
* [JWT](https://jwt.io/) - Token based authentication method
* [Lout](https://github.com/hapijs/lout) - API Documentation Generator


## Author

* **Leonardo Viveiros** 
